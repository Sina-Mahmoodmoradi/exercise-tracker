import {
    IconButton,
    List,
    ListItemButton,
    ListItemSecondaryAction,
    ListItemText,
} from '@mui/material'
import DeleteRoundedIcon from '@mui/icons-material/DeleteRounded'
import CheckCircleOutlineRoundedIcon from '@mui/icons-material/CheckCircleOutlineRounded'
import { CheckCircleRounded } from '@mui/icons-material'

const Items = ({ items, removeItem, checkItem }) => {
    return (
        <List>
            {items.map((item) => (
                <ListItemButton
                    onDoubleClick={() => checkItem(item.key)}
                    key={item.key}
                >
                    <ListItemText
                        primary={item.title}
                        secondary={`Sets: ${item.sets} - Reps: ${item.reps}`}
                    />
                    <ListItemSecondaryAction>
                        <IconButton color="primary">
                            {item.checked ? (
                                <CheckCircleRounded />
                            ) : (
                                <CheckCircleOutlineRoundedIcon />
                            )}
                        </IconButton>
                        <IconButton onClick={() => removeItem(item.key)}>
                            <DeleteRoundedIcon color="error" />
                        </IconButton>
                    </ListItemSecondaryAction>
                </ListItemButton>
            ))}
        </List>
    )
}

export default Items

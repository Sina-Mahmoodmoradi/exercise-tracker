import { Grid, Button, TextField, MenuItem } from '@mui/material'
import { makeStyles } from '@mui/styles'
import { useState } from 'react'
const useStyle = makeStyles((theme) => ({
    box: {
        margin: theme.spacing(2, 0),
        width: '100%',
        justifyContent: 'flex-end',
    },
}))
const AddItem = ({ addItem }) => {
    const onSubmit = () => {
        addItem(Exercise, sets, reps)
        setExercise('')
        setSets('')
        setReps('')
    }

    const classes = useStyle()
    const [Exercise, setExercise] = useState('')
    const [sets, setSets] = useState('')
    const [reps, setReps] = useState('')
    return (
        <div>
            <Grid container className={classes.box} spacing={1}>
                <Grid item xs={12} md={6}>
                    <TextField
                        label="Exercise"
                        value={Exercise}
                        onChange={(e) => setExercise(e.target.value)}
                        placeholder="Enter exercise name"
                        fullWidth
                    />
                </Grid>
                <Grid item xs={6} md={3}>
                    <TextField
                        fullWidth
                        label="Sets"
                        value={sets}
                        onChange={(e) => setSets(e.target.value)}
                        select
                    >
                        {[1, 2, 3, 4, 5].map((item) => (
                            <MenuItem key={item} value={item}>
                                {item}
                            </MenuItem>
                        ))}
                    </TextField>
                </Grid>
                <Grid item xs={6} md={3}>
                    <TextField
                        fullWidth
                        label="Reps"
                        value={reps}
                        onChange={(e) => setReps(e.target.value)}
                        select
                    >
                        {[5, 10, 15, 20, 30, 40].map((item) => (
                            <MenuItem key={item} value={item}>
                                {item}
                            </MenuItem>
                        ))}
                    </TextField>
                </Grid>
                <Grid item>
                    <Button variant="contained" onClick={onSubmit}>
                        ADD Item
                    </Button>
                </Grid>
            </Grid>
        </div>
    )
}

export default AddItem

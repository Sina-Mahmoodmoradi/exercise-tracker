import { MenuItem, TextField } from '@mui/material'
import { makeStyles } from '@mui/styles'

const useStyle = makeStyles((theme) => ({
    container: {
        display: 'flex',
        justifyContent: 'space-between',
        margin: theme.spacing(2, 0),
    },
    options: {
        width: '25vw',
        [theme.breakpoints.down('md')]: {
            width: '35vw',
        },
    },
}))
const colors = [
    { color: 'blue' },
    { color: 'orange' },
    { color: 'red' },
    { color: 'green' },
    { color: 'purple' },
    { color: 'yellow' },
    { color: 'pink' },
]
const themes = [{ theme: 'light' }, { theme: 'dark' }]

const Options = ({ onChangeColor, onChangeTheme, color, theme }) => {
    const classes = useStyle()

    const changeColor = (e) => {
        onChangeColor(e.target.value)
    }

    const changeTheme = (e) => {
        onChangeTheme(e.target.value)
    }
    return (
        <div className={classes.container}>
            <TextField
                className={classes.options}
                label="Color"
                value={color}
                onChange={changeColor}
                select
            >
                {colors.map((item) => {
                    return (
                        <MenuItem key={item.color} value={item.color}>
                            {item.color}
                        </MenuItem>
                    )
                })}
            </TextField>
            <TextField
                className={classes.options}
                label="Theme"
                value={theme}
                onChange={changeTheme}
                select
            >
                {themes.map((item) => {
                    return (
                        <MenuItem key={item.theme} value={item.theme}>
                            {item.theme}
                        </MenuItem>
                    )
                })}
            </TextField>
        </div>
    )
}

export default Options

import { Card } from '@mui/material'
import { makeStyles } from '@mui/styles'
import AddItem from './sections/AddItem'
import Options from './sections/Options'
import Items from './sections/Items'
import { useState } from 'react'

const useStyle = makeStyles((theme) => ({
    container: {
        width: '60vw',
        [theme.breakpoints.down('md')]: {
            width: '80vw',
        },
        padding: theme.spacing(2),
        margin: theme.spacing(4),
    },
}))

const Main = ({ onChangeColor, onChangeTheme, color, theme }) => {
    const classes = useStyle()
    const [items, setItems] = useState([
        {
            key: 1,
            title: 'this is first item',
            sets: 5,
            reps: 10,
            checked: false,
        },
        {
            key: 2,
            title: 'this is another item',
            sets: 4,
            reps: 20,
            checked: true,
        },
    ])
    const checkItem = (key) => {
        setItems(
            items.map((item) =>
                item.key === key ? { ...item, checked: !item.checked } : item
            )
        )
    }
    const removeItem = (key) => {
        setItems(items.filter((item) => item.key !== key))
    }

    const addItem = (text, sets, reps) => {
        let key = items.length + 1
        setItems([
            ...items,
            {
                key: key,
                title: text,
                sets: sets,
                reps: reps,
                checked: false,
            },
        ])
    }

    return (
        <Card className={classes.container} raised>
            <Options
                onChangeColor={onChangeColor}
                onChangeTheme={onChangeTheme}
                color={color}
                theme={theme}
            />
            <AddItem addItem={addItem} />
            {items.length > 0 ? (
                <Items
                    items={items}
                    checkItem={checkItem}
                    removeItem={removeItem}
                />
            ) : (
                'There are no items'
            )}
        </Card>
    )
}

export default Main

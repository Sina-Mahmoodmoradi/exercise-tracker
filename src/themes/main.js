import { createTheme } from '@mui/material/styles'
import {
    blue,
    green,
    orange,
    pink,
    purple,
    red,
    yellow,
} from '@mui/material/colors'

const Theme = (mode, color) =>
    createTheme({
        palette: {
            mode: mode,
            primary: {
                main:
                    color === 'green'
                        ? green[500]
                        : color === 'red'
                        ? red[500]
                        : color === 'pink'
                        ? pink[500]
                        : color === 'purple'
                        ? purple[500]
                        : color === 'blue'
                        ? blue[500]
                        : color === 'orange'
                        ? orange[500]
                        : color === 'yellow' && yellow[500],
            },
        },
    })

export default Theme

import Main from './components/Main'
import { CssBaseline } from '@mui/material'
import Theme from './themes/main'
import { ThemeProvider } from '@mui/material/styles'
import { useState } from 'react'
function App() {
    const [color, setColor] = useState('blue')
    const [mode, setMode] = useState('light')
    const onChangeTheme = (theme) => {
        setMode(theme)
    }
    const onChangeColor = (color) => {
        setColor(color)
    }
    return (
        <ThemeProvider theme={Theme(mode, color)}>
            <CssBaseline />
            <Main
                onChangeColor={onChangeColor}
                onChangeTheme={onChangeTheme}
                color={color}
                theme={mode}
            />
        </ThemeProvider>
    )
}

export default App
